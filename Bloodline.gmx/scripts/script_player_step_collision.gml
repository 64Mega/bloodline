// Collision with level and gravity

// This line accidentally created a ceiling crawl ability. Maybe use?
// g1 = collision_rectangle(x-8,y-18,x+8,y+1+vspeed, obj_block16, false, true);

g1 = collision_rectangle(x-4,y-15,x+4,y+1+vspeed, obj_block16, false, true);
if(vspeed < 0) 
{
    g1 = collision_rectangle(x-4,y-15-abs(vspeed)-1,x+4,y, obj_block16, false, true);
    isInAir = true;
}
if(g1 == noone) 
{
    gravity = BASE_GRAVITY;
} else
{
    if(vspeed < 0) 
    {
        old_vspeed = vspeed;
        move_contact_solid(90, old_vspeed);
        move_outside_solid(270, old_vspeed);
        vspeed = vspeed / 2;  
    }
    if(vspeed > 0) 
    {
        move_contact_solid(270, vspeed);
        move_outside_solid(90, vspeed);
        vspeed = 0;
        gravity = 0;
        isInAir = false;
    }
}
if(place_free(x,y+1))
{
    isInAir = true;
    falltime += 1;
}

// Extra catch for ceiling bug 
if(vspeed == 0 && place_free(x, y+1)) // && !ceilingAbility?
{
    y += 1; // Clip out of block
}

// Limit vertical downwards velocity
if(vspeed > BASE_VSPEED_CAP) 
{
    vspeed = BASE_VSPEED_CAP;
}
