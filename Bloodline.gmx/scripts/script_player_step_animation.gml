if(isInAir) 
{
    if(vspeed < 0) 
    {
        if(sprite_index == spr_player_jump)
        {
            if(image_index >= image_number-1)
            {
                image_speed = 0;
            }   
        } else
        {
            sprite_index = spr_player_jump;
            image_speed = 0.25;
            image_index = 0;
        }
    }
    if(vspeed > 0)
    {
        if(sprite_index == spr_player_fall)
        {
            if(image_index >= image_number-1)
            {
                image_speed = 0;
            }   
        } else
        {
            sprite_index = spr_player_fall;
            image_speed = 0.25;
            image_index = 0;
        }
    }
}
else // Not In Air
{
    if(isMoving) 
    {
        sprite_index = spr_player_walk;
        image_speed = 0.25;
    } else // Not Moving
    {
        sprite_index = spr_player_template;   
    }
}
