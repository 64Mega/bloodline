// Movement Control

// Exit code if an instance of the pause menu exists.
if(instance_exists(obj_inventory))
{
    exit;
}

if(keyboard_check(vk_left) || keyboard_check(vk_right)) 
{
    isMoving = true;
} else
{
    isMoving = false;
}

if(keyboard_check(vk_left))
{
    image_xscale = -1;
    d1 = collision_rectangle(x, y - 16, x - 8 - BASE_WALK_SPEED, y - 1, obj_block16, false, true); 
    if(d1 == noone && (x - 14 > 0)) 
    {
        x -= BASE_WALK_SPEED;    
    } else 
    {
        move_contact_solid(180, BASE_WALK_SPEED);
        move_outside_solid(0, BASE_WALK_SPEED);
    }
} else if(keyboard_check(vk_right))
{
    image_xscale = 1;
    d1 = collision_rectangle(x, y - 16, x + 8 + BASE_WALK_SPEED, y - 1, obj_block16, false, true); 
    if(d1 == noone && (x + 14 < room_width)) 
    {
        x += BASE_WALK_SPEED;    
    } else 
    {
        move_contact_solid(0, BASE_WALK_SPEED);
        move_outside_solid(180, BASE_WALK_SPEED);
    }
}

if(keyboard_check_pressed(ord('A')))
{
    if(isInAir == false && place_free(x, y-BASE_JUMP_HEIGHT-1))
    {
        vspeed = -BASE_JUMP_HEIGHT;
    } 
    if(isInAir == true && falltime < 4 && vspeed >= 0)
    {
        vspeed = -BASE_JUMP_HEIGHT;       
    }   
}
if(keyboard_check_released(ord('A')))
{
    if(isInAir == true)
    {
        if(vspeed < 0) 
        {
            //vspeed = vspeed / 4;
        }
    }
}

// Stamina regen
if(stamina < stamina_max) {
    if(stamina_cooldown <= 0) {
        stamina += STAMINA_RECHARGE_RATE;
    } else 
    {
        stamina_cooldown -= 1;
    }
} else 
{
    stamina = stamina_max;
}

// Basic projectile
if(keyboard_check_pressed(ord('S')))
{
    if(stamina >= 10) 
    {
        stamina -= 20;
        obj = instance_create(x,y-12,obj_atk_basic);
        obj.dir = image_xscale;
        if(stamina < 0) { stamina = 0; }
    }    
}
