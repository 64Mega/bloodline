isInAir = false;
isWalking = false;

hp = 10;

BASE_WALK_SPEED = 2;
BASE_JUMP_HEIGHT = 6;
BASE_GRAVITY = 0.25;
BASE_VSPEED_CAP = 8;

SPEED_MODIFIER = 0;
JUMP_HEIGHT_MODIFIER = 0;

STAMINA_RECHARGE_RATE = 0.5;

stamina_max = 100;
stamina_cooldown  = 0;
stamina = 0;
falltime = 0;

elements_fire = 2;
elements_air = 3;
elements_earth = 4;
elements_water = 0;

// Create HUD

if(!instance_exists(hud))
{
    instance_create(0,0,hud);
}

// Snap view to player
view_xview[0] = x - 200;
view_yview[0] = y - 112;
